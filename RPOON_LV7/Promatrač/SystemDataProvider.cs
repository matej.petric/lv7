﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV7.Promatrač
{
    class SystemDataProvider : SimpleSystemDataProvider
    {
        private float previousCPULoad;
        private float previousRAMAvailable;
        private float significantDifference = 0.1f;
        public SystemDataProvider() : base()
        {
            this.previousCPULoad = this.CPULoad;
            this.previousRAMAvailable = this.AvailableRAM;
        }
        public float GetCPULoad()
        {
            float currentLoad = this.CPULoad;
            if (currentLoad != this.previousCPULoad)
            {
                if(IsCPULoadDifferenceSignificant())
                {
                    this.Notify();
                }
                this.previousCPULoad = currentLoad;
            }
            return currentLoad;
        }
        public float GetAvailableRAM()
        {
            float currentAvailableRAM = this.AvailableRAM;
            if(currentAvailableRAM != this.previousRAMAvailable)
            {
                if(IsAvailableRAMDifferenceSignificant())
                {
                    this.Notify();
                }
                this.previousRAMAvailable = currentAvailableRAM;
            }
            return currentAvailableRAM;
        }
        
        private bool IsCPULoadDifferenceSignificant()
        {
            float quotient = CPULoad / previousCPULoad;

            if(quotient<1)
            {
                return quotient > significantDifference;
            }
            else
            {
                return quotient > 1 + significantDifference;
            }
        }

        private bool IsAvailableRAMDifferenceSignificant()
        {
            float quotient = AvailableRAM / previousRAMAvailable;

            if (quotient < 1)
            {
                return quotient > significantDifference;
            }
            else
            {
                return quotient > 1 + significantDifference;
            }
        }
    }
}
