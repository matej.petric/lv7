﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV7.Posjetitelj
{
    class Cart : IItem
    {
        private List<IItem> items;

        public Cart()
        {
            items = new List<IItem>();
        }

        public void AddItem(IItem Item)
        {
            items.Add(Item);
        }

        public void RemoveItem(IItem Item)
        {
            items.Remove(Item);
        }

        public double Accept(IVisitor visitor)
        {
            double sum = 0;
            foreach(IItem item in items)
            {
                sum += item.Accept(visitor);
            }
            return sum;
        }
    }
}
