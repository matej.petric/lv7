﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV7.Posjetitelj
{
    class RentVisitor : IVisitor
    {
        private const double RentPricePercentage = 0.1;
        public double Visit(DVD DVDItem)
        {
            //return double.NaN;
            return DVDItem.Price * RentPricePercentage;
        }
        public double Visit(VHS VHSItem)
        {
            return VHSItem.Price * RentPricePercentage;
        }
        public double Visit(Book book)
        {
            return book.Price * RentPricePercentage;
        }
    }
}
