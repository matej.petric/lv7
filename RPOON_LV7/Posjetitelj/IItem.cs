﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV7.Posjetitelj
{
    interface IItem
    {
        double Accept(IVisitor visitor);
    }
}
