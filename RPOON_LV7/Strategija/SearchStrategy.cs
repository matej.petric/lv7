﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV7.Strategija
{
    interface SearchStrategy
    {
        bool Search(double[] Array, double Number);
    }
}
