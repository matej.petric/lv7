﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV7.Strategija
{
    class BinarySearch : SearchStrategy
    {
        public bool Search(double[] Array, double Number)
        {
            int lowBound = 0;
            int highBound = Array.Length-1;
            int middle = (highBound-lowBound) / 2;
            while(lowBound<=highBound)
            {
                middle = lowBound+(highBound - lowBound) / 2;
                if (Number == Array[middle])
                {
                    return true;
                }
                if (Number > Array[middle])
                {
                    lowBound = middle+1;
                }
                if (Number < Array[middle])
                {
                    highBound = middle-1;
                }
            }
            return false;
        }
    }
}
