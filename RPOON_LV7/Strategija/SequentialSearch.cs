﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV7.Strategija
{
    class SequentialSearch : SearchStrategy
    {
        public bool Search(double[] Array, double Number)
        {
            for(int i=0;i<Array.Length;i++)
            {
                if(Array[i]==Number)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
