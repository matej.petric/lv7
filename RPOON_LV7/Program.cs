﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPOON_LV7.Strategija;
using RPOON_LV7.Promatrač;
using RPOON_LV7.Posjetitelj;

namespace RPOON_LV7
{
    class Program
    {
        static void Main(string[] args)
        {
            //PRVI
            double[] nums = new double[] { 42,54,521,52,1,5,3};
            NumberSequence numbers = new NumberSequence(nums);
            numbers.SetSortStrategy(new BubbleSort());
            numbers.Sort();
            Console.Write(numbers.ToString());  Console.WriteLine();

            //DRUGI
            numbers.SetSearchStrategy(new SequentialSearch());
            Console.WriteLine("Number found on "+numbers.Search(5)+". place.");
            numbers.SetSearchStrategy(new BinarySearch());
            Console.WriteLine("Number found on " + numbers.Search(5) + ". place."); Console.WriteLine();

            ////TRECI
            //SystemDataProvider systemDataProvider = new SystemDataProvider();
            //systemDataProvider.Attach(new ConsoleLogger());
            //while(true)
            //{
            //    systemDataProvider.GetCPULoad();
            //    System.Threading.Thread.Sleep(1000);
            //}

            //PETI
            BuyVisitor buyVisitor = new BuyVisitor();
            Book book = new Book("Harry Potter",5.20);
            DVD dvd = new DVD("Razna glazba", DVDType.SOFTWARE, 10.20);
            VHS vhs = new VHS("Asterix & Obelix", 2.30);
            List<IItem> items = new List<IItem>() { book, dvd, vhs };
            foreach(IItem item in items)
            {
                Console.WriteLine(item);
                Console.WriteLine(" -> Price with tax: " + item.Accept(buyVisitor));
            }   Console.WriteLine();

            //SESTI
            RentVisitor rentVisitor = new RentVisitor();
            foreach (IItem item in items)
            {
                Console.WriteLine(item);
                Console.WriteLine(" -> Price for rent: " + item.Accept(rentVisitor));
            }
            Console.WriteLine();

            //SEDMI
            Cart cart = new Cart();
            foreach(IItem item in items)
            {
                cart.AddItem(item);
            }
            Console.WriteLine("Total price for rent: " + cart.Accept(rentVisitor));
        }
    }
}
